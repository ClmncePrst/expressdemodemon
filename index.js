// importe le module Express
const express = require('express');
const app = express();
// on utilisera le port 3000 pour accéder au serveur
const port = 3000;

// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
  // envoie une réponse 'Hello World!' au client
  res.send('Hello World!');
})

// définition d'une nouvelle route GET sur l'URL ('/data')
app.get('/data', (req, res) => {
    // envoie un objet JSON en réponse
    res.send({ name: 'Pikachu', power: 20, life: 50 });
  });
  
  

// démarrage du serveur sur le port défini
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
